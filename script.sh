#!/bin/bash

#Variables
TRIGGER_TOKEN=$1
PROJECT_ID=$2
PRIVATE_TOKEN=$3
JOB_NAME=$4

#Trigger the Pipeline
curl --request POST "https://gitlab.com/api/v4/projects/$PROJECT_ID/trigger/pipeline?token=$TRIGGER_TOKEN&ref=main" > pipeline_id.txt

pipeline_ID=$(cat pipeline_id.txt | cut -d "," -f1 | cut -d ":" -f2); echo $pipeline_ID


#Get the list of jobs
curl --globoff --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" "https://gitlab.com/api/v4/projects/$PROJECT_ID/pipelines/$pipeline_ID/jobs" > jobs.txt

#Convert the jobs.txt file from a single line string to multi line seperated by ","
sed 's/,/\n/g' jobs.txt >> temp_jobs.txt

#Get the line number of the Job name
job_name_line_number=$(grep -n $JOB_NAME temp_jobs.txt | cut -d ":" -f1)

#Store the line number of the corresponding job id
let id_line_number=job_name_line_number-3

#Get the job id
job_id=$(awk "{ if(NR==$id_line_number) print; }" temp_jobs.txt | cut -d ":" -f2); echo $job_id

#Play the job using the above job id
curl -X POST --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" "https://gitlab.com/api/v4/projects/$PROJECT_ID/jobs/$job_id/play"




